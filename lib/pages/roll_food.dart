import 'dart:async';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import './restaurant_detail.dart';

class RollFood extends StatefulWidget {
  static String tag = 'roll-food';

  @override
  _RollFoodState createState() => _RollFoodState();
}

class _RollFoodState extends State<RollFood> with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    this._animationController = AnimationController(vsync: this, duration: const Duration(seconds: 2));
    this._animation = CurvedAnimation(parent: this._animationController, curve: Curves.elasticOut);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: RotationTransition(
            turns: _animation,
            child: SizedBox(
              width: 200.0,
              height: 200.0,
              child: RaisedButton.icon(
                onPressed: () {
                  setState(() {
                    _animationController.repeat(period: const Duration(seconds: 2));
                    new Timer(const Duration(seconds: 2), () {
                      if (_animationController != null){
                        _animationController.stop();
                      }
                      Navigator.of(context).pushNamed(RestaurantDetail.tag);
                    });
                  });
                },
                icon: Icon(FontAwesomeIcons.dice,
                    color: Colors.white, size: 110.0
                ),
                color: Colors.lightBlueAccent,
                splashColor: Colors.blueGrey,
                label: Text(''),
                elevation: 5.0,
                shape: CircleBorder(),
              ),
            ),
          )
      ),
    );
  }
}
