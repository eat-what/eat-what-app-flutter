import 'package:flutter/material.dart';
import 'package:eat_what/components/round_icon_button.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:geolocator/geolocator.dart';
import 'package:eat_what/common/api.dart';
import 'package:eat_what/common/state.dart' as AppState;
import 'package:eat_what/models/restaurant.dart';
import 'package:eat_what/components/restaurant_card.dart';

class RestaurantDetail extends StatefulWidget {
  static String tag = 'restaurant-detail';

  @override
  _RestaurantDetail createState() => _RestaurantDetail();
}

class _RestaurantDetail extends State<RestaurantDetail> {
  Restaurant _restaurant;
  Position _position;
  final API _api = API();

  @override
  void initState() {
    super.initState();
    _getRestaurant();
  }

  void _getRestaurant() async {
    this.setState(() => this._restaurant = null);
    Position p = await AppState.State.getCurrentLocation();
    Restaurant r = await _api.decideFood(
        lat: p.latitude, long: p.longitude, commute: 'driving');
    this.setState(() {
      _restaurant = r;
      _position = p;
    });
  }

  void _launchMaps(String address) async {
    address = address.replaceAll(' ', '+');
    String appleUrl = 'https://maps.apple.com/?q=$address';
    String googleUrl = 'comgooglemaps://?q=$address';

    if (await canLaunch("comgooglemaps://")) {
      await launch(googleUrl);
    } else if (await canLaunch(appleUrl)) {
      await launch(appleUrl);
    } else {
      showDialog(context: context, builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Failed to lauch map!'),
          content: Text('Unable to launch google or apple map'),
          actions: <Widget>[
            FlatButton(
              child: Text('Close'),
              onPressed: () => Navigator.of(context).pop(),
            )
          ],
        );
      });
    }
  }

  Widget _buildAppBar() {
    return new AppBar(
      iconTheme: IconThemeData(color: Colors.black, size: 30.0),
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      title: new FlutterLogo(
        size: 30.0,
        colors: Colors.lightBlue,
      ),
      actions: <Widget>[
        new IconButton(
          icon: new Icon(Icons.settings),
          onPressed: () {
            // TODO:
          },
        )
      ],
    );
  }

  Widget _buildBottomBar() {
    return BottomAppBar(
      color: Colors.transparent,
      elevation: 0.0,
      child: new Padding(
        padding: const EdgeInsets.all(16.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new RoundIconButton.large(
              icon: Icons.refresh,
              iconColor: Colors.red,
              onPressed: () => this._getRestaurant(),
            ),
            new RoundIconButton.large(
              icon: Icons.time_to_leave,
              iconColor: Colors.green,
              onPressed: () => this._launchMaps(_restaurant.location),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: _buildAppBar(),
      body: _restaurant != null
          ? RestaurantCard(
          name: this._restaurant.name, photos: this._restaurant.photos)
          : Center(child: CircularProgressIndicator()),
      bottomNavigationBar: _buildBottomBar(),
    );
  }
}
