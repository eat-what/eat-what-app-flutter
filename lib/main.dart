import 'package:flutter/material.dart';
import 'pages/login.dart';
import 'pages/splash_screen.dart';
import 'pages/restaurant_detail.dart';
import 'pages/roll_food.dart';

void main() => runApp(EatWhat());

class EatWhat extends StatelessWidget {
  // This widget is the root of your application.
  final Map<String, WidgetBuilder> routes = {
    Login.tag: (BuildContext context) => Login(),
    RestaurantDetail.tag: (BuildContext context) => RestaurantDetail(),
    RollFood.tag: (BuildContext context) => RollFood(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Eat What',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.lightBlue,
      ),
      home: SplashScreen(),
      routes: routes,
    );
  }
}
