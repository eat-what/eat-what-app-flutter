import 'package:meta/meta.dart';

class Review {
  final String text, link, timeCreated;
  final int rating;

  Review({
    @required this.text,
    @required this.link,
    @required this.timeCreated,
    @required this.rating,
  });

  Review.fromJson(Map json)
      : text = json['text'],
        link = json['link'],
        timeCreated = json['timeCreated'],
        rating = json['rating'];
}
