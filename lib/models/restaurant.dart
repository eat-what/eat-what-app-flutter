import 'package:meta/meta.dart';
import 'package:eat_what/models/review.dart';

class Restaurant {
  final String id, name, location;
  final num rating;
  final int reviewCount;
  final List<Review> reviews;
  final List<String> photos;

  Restaurant({
    @required this.id,
    @required this.name,
    @required this.location,
    @required this.rating,
    @required this.reviewCount,
    @required this.photos,
    this.reviews,
  });

  factory Restaurant.fromJson(Map json) {
    List<Review> reviews;
    if (json['reviews'] != null) {
      Iterable i = json['reviews'];
      reviews = i.map((model)=> Review.fromJson(model)).toList();
    }
    List<String> photos;
    if (json['photos'] != null) {
      Iterable i = json['photos'];
      photos = i.map((model)=> model.toString()).toList();
    }
    return new Restaurant(
      id: json['id'],
      name: json['name'],
      location: json['location'],
      rating: json['rating'],
      reviewCount: json['reviewCount'],
      photos: photos,
      reviews: reviews,
    );
  }
}
