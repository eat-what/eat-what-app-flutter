import 'package:geolocator/geolocator.dart';

class State {
  static final Geolocator _locator =  Geolocator();

  static Future<Position> getCurrentLocation() async {
    return _locator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }
}
