import 'dart:convert';
import 'dart:core';
import 'package:http/http.dart' as http;
import 'package:eat_what/models/restaurant.dart';
import 'package:eat_what/config/config.dart';

class API {
  static final API _instance = API._internal();
  static final http.Client _httpClient = http.Client();

  factory API() => _instance;

  API._internal();

  Future<Restaurant> decideFood({double lat, double long, String commute}) async {
    Uri uri = Uri.https(Config.server, Config.apiBase + 'decide-food', {'long': long.toString(), 'lat': lat.toString(), 'commute': commute});
    http.Response resp = await _httpClient.get(uri);
    if (resp.statusCode != 200) {
      return Future.error(resp.body);
    }
    Map<String, dynamic> json = jsonDecode(resp.body);
    return Restaurant.fromJson(json['result']);
  }
}
